# VIM full disable visual mode globally
#
```sh
sudo vim /etc/vim/vimrc.local
set mouse=
set ttymouse=
```